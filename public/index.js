var userLatitude, userLongitude; // Declare these as global variables
var hasVoted = false;
window.onload = function() {
    var queryParams = new URLSearchParams(window.location.search);
    var id = queryParams.get('stickerId');
    var imgPath = 'stickers/' + id + '.svg';
    document.getElementById('stickerImage').setAttribute('src', imgPath);
    checkLoadedFont('body');
    var currentFontDebugMessage = console.log(document.getElementById('stickerImage').style.fontFamily)
    gsap.from("#stickerImage", {duration: 1.5, scale: 0, ease: "elastic.out(1, 0.3)"});
    gsap.from("button", {duration: 1, opacity: 0, stagger: 0.2, y: 50});
    gsap.from("textarea", {duration: 1, opacity: 0, x: -50});
    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function(position) {
                            userLatitude = position.coords.latitude;
                            userLongitude = position.coords.longitude;
                        }, function(error) {
                            console.error("Error Code = " + error.code + " - " + error.message);
                            // Handle error or set default values if needed
                            userLatitude = null;
                            userLongitude = null;
                        });
                    } else {
                        console.error("Geolocation is not supported by this browser.");
                        // Handle case where geolocation is not supported
                        userLatitude = null;
                        userLongitude = null;
                    }
                };



function getLocationAndSendVote(isLike) {
                sendVote(isLike, userLatitude, userLongitude); // Use stored location
            }

function checkLoadedFont(elementSelector) {
    const el = document.querySelector(elementSelector);
    if (!el) {
        console.warn('Element not found');
        return;
    }
    const style = window.getComputedStyle(el);
    const fontFamily = style.fontFamily.split(',')[0]; // Gets the first font in the font-family list
    console.log(`The current loaded font for ${elementSelector} is:`, fontFamily);
    }
    
    // Usage: pass in any valid CSS selector for the element you want to check
    checkLoadedFont('body');

function startHeartRain() {
    const numberOfHearts = 50; // Number of hearts you want to rain down

    for (let i = 0; i < numberOfHearts; i++) {
        let heart = document.createElement('div');
        heart.className = 'heart';
        document.head.appendChild(heart);

        // Randomize various aspects of the heart
        let startLeft = Math.random() * window.innerWidth;
        let endLeft = Math.random() * window.innerWidth;
        let duration = 3 + Math.random() * 3; // Duration between 2 to 5 seconds
        let delay = Math.random() * 2; // Delay up to 2 seconds

        // GSAP animation for the heart
        gsap.fromTo(heart, 
            {
                x: startLeft,
                y: -50, // Start above the screen
                opacity: 1,
                rotation: Math.random() * 360 // Random rotation for variety
            },
            {
                x: endLeft,
                y: window.innerHeight + 50, // End below the screen
                opacity: 0,
                rotation: Math.random() * 360,
                duration: duration,
                delay: delay,
                ease: 'bounce.out', // Bounce effect
                onComplete: function() {
                    heart.remove(); // Remove heart after animation completes
                }
            }
        );
    }
}


function displayThankYouMessage() {
    var header = document.querySelector('h1'); // Assuming the header text is in <h1>
    var headerText = header.innerText;
    var index = headerText.length;

    function eraseText() {
        if (index > 0) {
            // Remove one character at a time
            header.innerText = headerText.substr(0, index);
            index--;
            setTimeout(eraseText, 50); // Set delay for typewriter effect
        } else {
            // Once header is erased, display the thank you message
            typewriterEffect("Thank you for voting!", document.querySelector('h1'), 50);
            startHeartRain(); // Start the heart rain animation
        }
    }
    eraseText(); // Start erasing the text
}



function sendVote(isLike, lat, lon) {
    var queryParams = new URLSearchParams(window.location.search);
    var id = queryParams.get('stickerId');
    var stickerId = parseInt(id);

    // Ensure hasVoted is checked before the asynchronous call
    if (hasVoted){
        console.error('You cant vote twice.');
        return;
    }

    var message = document.getElementById('message').value;

    // Define postData with the current message
    var postData = {
        isLike: isLike,
        stickerId: stickerId,
        location: lat ? {lat: lat, lon: lon} : null,
        message: message, // Use the current value of the message
        ipAddress: null
    };



    getUserIP(ipAddress => {
        postData["ipAddress"] = ipAddress; // Add the IP address to postData
        // Now send the fetch request here, inside the callback
        submitVote(postData);
    }, error => {
        console.error('Error obtaining IP address:', error);
        submitVote(postData); // Submit vote even if IP fetch fails
    });
}

function submitVote(postData) {
    gsap.to(["#stickerContainer", "#message", "button", "#disclaimer"], {
        duration: 1,
        opacity: 0,
        onComplete: displayThankYouMessage
    });

    showProgressModal(5);
    console.log(postData);
    fetch('https://script.google.com/macros/s/AKfycbwK76GAC9aKLomXSXZ28rU-luQf1qoPGm9UExojJ6MQfsCezmJu0E-f-GL5m1kZ8ahF/exec', {
        method: 'POST',
        mode: 'no-cors',
        redirect: 'follow',
        body: JSON.stringify(postData),
        headers: new Headers({
            'Content-Type': 'application/json'
        })
    }).then(function(response) {
        console.log('Success:', response);
        updateProgressBar(5, true);
        setTimeout(function() {
            hideProgressModal();
            displayThankYouMessage();
        }, 1000);
    }).catch(function(error) {
        console.error('Error:', error);
        hideProgressModal();
    });

    hasVoted = true;
}


function typewriterEffect(text, target, speed) {
    let currentIndex = 0;
    function addCharacter() {
        if (currentIndex < text.length) {
            target.innerText = text.substring(0, currentIndex + 1);
            currentIndex++;
            setTimeout(addCharacter, speed);
        }
    }
    addCharacter();
}

function showProgressModal(steps) {
    var modal = document.getElementById('progressModal');
    var bar = document.getElementById('progressBar');

    bar.innerHTML = ''; // Clear any existing content
    for (let i = 0; i < steps; i++) {
        let heart = document.createElement('img');
        heart.src = '/rate-my-sticker/heart.svg';
        heart.style.filter = 'grayscale(100%)';
        heart.style.maxWidth = '100%';
        heart.style.maxHeight = '100%';
        bar.appendChild(heart);
    }

    modal.style.display = 'flex';
}

function updateProgressBar(step, complete = false) {
    var hearts = document.getElementById('progressBar').children;
    for (let i = 0; i < step; i++) {
        if (hearts[i]) {
            hearts[i].style.filter = complete ? 'none' : 'grayscale(0%)'; // Remove grayscale if complete
        }
    }
}

function hideProgressModal() {
    var modal = document.getElementById('progressModal');
    gsap.to(modal, {
        duration: 0.5,
        opacity: 0,
        onComplete: function() {
            modal.style.display = 'none';
            modal.style.opacity = 1; // Reset for next time
        }
    });
}

function getUserIP(onSuccess, onError, timeout = 1500) {
    const controller = new AbortController();
    const signal = controller.signal;
    
    // Start a timer that will abort the fetch if time exceeds the timeout
    const fetchTimeout = setTimeout(() => {
        controller.abort();
    }, timeout);

    fetch('https://api.ipify.org?format=json', {
        method: 'GET',
        mode: 'cors',
        redirect: 'follow',
        signal: signal
    })
        .then(response => {
            clearTimeout(fetchTimeout); // Clear the timeout on successful response
            return response.json();
        })
        .then(data => onSuccess(data.ip))
        .catch(error => {
            clearTimeout(fetchTimeout); // Clear the timeout also in case of error to clean up
            onError(error);
        });
}
